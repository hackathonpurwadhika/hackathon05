<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request; //penambahan buat log out redirect

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $redirectAfterLogout = 'auth/login';
    //protected $redirectAfterLogout = '/index2';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // public function logout()
    // {
    //     return redirect(property_exists($this, 'redirectAfterLogout') ? $this- >redirectAfterLogout : '/index');
    // }

    public function logout(Request $request) //penambahan buat log out
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/index');
        //return redirect('/index2');
    }

}
