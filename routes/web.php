<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//TEST2
//TEST1shghsghsjhsjhs
//asdasd
//86d96659e7f9a16c3c6c0c58fb872b8bc93d716c
// Route::get('/', function () {
//     return view('welcome');
// });
//

Route::get('/about', function () {
    return view('about');
});

Route::get('/care', function () {
    return view('care');
});

Route::get('/codes', function () {
    return view('codes');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/faqs', function () {
    return view('faqs');
});

Route::get('/hold', function () {
    return view('hold');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('/kitchen', function () {
    return view('kitchen');
});


Route::get('/login', function () {
    return view('login');
});

Route::get('/offer', function () {
    return view('offer');
});

Route::get('/register', function () {
    return view('register');
});

Route::get('/shipping', function () {
    return view('shipping');
});

Route::get('/single', function () {
    return view('single');
});

Route::get('/terms', function () {
    return view('terms');
});

Route::get('/wishlist', function () {
    return view('wishlist');
});

Route::get('/test', function () {
    return view('test');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('index2');

//

// Route::group(['prefix' => 'shit', 'middleware' => 'auth'], function ()
//      {
// Route::get('/', function() {
//         return view('shit.abislogin');
//     })->name('shit.abislogin');

Route::resource('product','ProductsController');
Route::resource('category','CategoriesController');

// });
// {
//     Route::get('/', function() {
//         return view('index2');
//     })->name('index2');
